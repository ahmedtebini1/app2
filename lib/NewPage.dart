import 'package:flutter/material.dart';

var flume = 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Pulitzer2018-portraits-kendrick-lamar.jpg/260px-Pulitzer2018-portraits-kendrick-lamar.jpg';
var flume1 = 'https://ksassets.timeincuk.net/wp/uploads/sites/55/2018/05/Kendrick-Lamar-920x584.jpg';
var flume2 = 'https://www.clique.tv/wp-content/uploads/2017/12/home-clique-tv-kendrick-lamar-disque-dur.jpg';
var flume3 = 'https://static.lexpress.fr/medias_10474/w_1365,h_593,c_crop,x_0,y_241/w_960,h_540,c_fill,g_north/v1434964387/kendrick-lamar-2_5363111.jpg';
var flume4 = 'https://www.clique.tv/wp-content/uploads/2017/03/Kendrick-Lamar.jpg';
var flume5 = 'https://www.rollingstone.com/wp-content/uploads/2018/06/kendrick-lamar-pulitzer-long-way-to-go-090d5e27-b581-4338-aad3-31ebf6b8b497.jpg?resize=900,600&w=1440';

var ll = 'https://www.availableideas.com/wp-content/uploads/2016/02/Mic-And-Headphones-iPhone-6-Plus-HD-Wallpaper.jpg';

class NewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF090e42),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 32.0,
            ),
            CustomTextField(),
            SizedBox(
              height: 32.0,
            ),
            Text(
              'Collections',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 38.0),
            ),
            SizedBox(
              height: 32.0,
            ),
            Row(
              children: <Widget>[
                ItemCard('assets/download.jpg', 'DAMN'),
                SizedBox(
                  width: 16.0,
                ),
                ItemCard('assets/download.jpg', 'DAMN'),
              ],
            ),
            Row(
              children: <Widget>[
                ItemCard('assets/download.jpg', 'DAMN'),
                SizedBox(
                  width: 16.0,
                ),
                ItemCard('assets/download.jpg', 'DAMN'),
              ],
            ),
            SizedBox(
              width: 16.0,
            ),
            Text(
              'Recomend',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 38.0),
            ),
            SongItem('HUMBLE', 'Kendrick', flume),
            SongItem('DNA', 'Kendrick', flume1),
            SongItem('FEAR', 'Kendrick', flume2),
            SongItem('YEAH', 'Kendrick', flume3),
            SongItem('XXX', 'Kendrick', flume4),
            SongItem('LOVE', 'Kendrick', flume5),
          ],
        ),
      ),
    );
  }
}

class SongItem extends StatelessWidget {
  final title;
  final artist;
  final image;

  SongItem(this.title, this.artist, this.image);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailedScreen(title, artist, image)));
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 26.0),
        child: Row(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 80.0,
                  width: 80.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.network(
                      image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  height: 80.0,
                  width: 80.0,
                  child: Icon(
                    Icons.play_circle_filled,
                    color: Colors.white.withOpacity(0.7),
                  ),
                ),
              ],
            ),
            SizedBox(
              width: 16.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 24.0),
                ),
                Text(
                  artist,
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.5), fontSize: 18.0),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class ItemCard extends StatelessWidget {
  final image;
  final title;
  ItemCard(this.image, this.title);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Column(
        children: <Widget>[
          Container(
            height: 150,
            width: 200,
            child: Stack(
              children: <Widget>[
                ClipRect(
                    child: Image.asset(
                  image,
                  fit: BoxFit.cover,
                )),
                Positioned(
                  right: 16.0,
                  top: 16.0,
                  child: Icon(
                    Icons.bookmark,
                    color: Colors.white.withOpacity(0.6),
                    size: 20.0,
                  ),
                )
              ],
            ),
          ),
          Text(
            title,
            style: TextStyle(color: Colors.white, fontSize: 20.0),
          ),
        ],
      ),
    );
  }
}

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey.withOpacity(0.16),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.search,
            color: Colors.white,
          ),
          Expanded(
              child: TextField(
            decoration: InputDecoration(
              hintText: 'Search misic...',
              hintStyle: TextStyle(color: Colors.grey),
            ),
          ))
        ],
      ),
    );
  }
}

class DetailedScreen extends StatelessWidget {
  final title;
  final artist;
  final image;
  DetailedScreen(this.title, this.artist, this.image);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF090E49),
      appBar: AppBar(),
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 500.0,
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: 500.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(image), fit: BoxFit.cover),
                      ),
                    ),
                    Container(
                      height: 500.0,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              Color(0xFF090E49).withOpacity(0.4),
                              Color(0xFF090E49)
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 48.0,),


                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                          children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.1),borderRadius: BorderRadius.circular(50.0),


                            ),
                            child:   Icon(Icons.arrow_drop_down, color: Colors.white,

                            ),


                          ),
                            Column(
                              children: <Widget>[
                                Text('PLAYLIST', style: TextStyle(color: Colors.white),),
                                Text('Best Vibes of the Week', style: TextStyle(color: Colors.white),),
                              ],
                            ),
                            Icon(Icons.playlist_add, color: Colors.white,),
                          ],
                        ),

                          Spacer(),
                          Text(title , style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 32.0 ),),
                          Text(artist, style: TextStyle(color: Colors.white.withOpacity(0.6),fontSize: 18.0),),
                          SizedBox(height: 32.0,),





                      ],),
                    )
                  ],
                ),
              )



            ],
          ),
    SizedBox(height: 8.0,),


    Slider(
      onChanged: (double value) {},
      value: 0.2,
      activeColor: Color(0xFFff6b80),


    ),
Padding(
  padding: const EdgeInsets.symmetric(horizontal: 18.0),
  child:   Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,

    children: <Widget>[

    Text('2:10',style: TextStyle(color: Colors.white.withOpacity(0.8)),),
    Text('-3:59',style: TextStyle(color: Colors.white.withOpacity(0.8)),),



  ],),
),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[

              Icon(Icons.fast_rewind, color: Colors.white54,),
              SizedBox(width: 16.0,),
              Container(
    decoration: BoxDecoration(

    color: Color(0xFFff6b80),
      borderRadius: BorderRadius.circular(50.0)

    ),




                child: Icon(Icons.play_arrow, size: 30.0, color: Colors.white,),


              ) ,

              
              SizedBox(width: 16.0,),
              Icon(Icons.fast_forward, color: Colors.white54,),




            ],)











        ],
      ),
    );
  }
}
