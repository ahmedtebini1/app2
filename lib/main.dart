import 'package:flutter/material.dart';
import 'package:flutter_app2/NewPage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: new ThemeData(primarySwatch: Colors.deepOrange),
        home: new HomePage(),
        routes: <String, WidgetBuilder>{
          "/NewPage": (BuildContext context) => new NewPage()
        });
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.black,
      appBar: new AppBar(
        title: new Text('MUSIC APP'),
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("sahli houssem"),
              accountEmail: new Text("houssem@gmail.com"),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.limeAccent,
                child: new Text("H"),
              ),
            ),
            new ListTile(
              title: new Text("MUSIC"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => Navigator.of(context).pushNamed("/NewPage"),
            ),
            new ListTile(
              title: new Text("RADIO"),
              trailing: new Icon(Icons.add_circle_outline),
            ),
            new ListTile(
              title: new Text("PARTY"),
              trailing: new Icon(Icons.adjust),
            ),
            new Divider(),
            new ListTile(
              title: new Text("close"),
              trailing: new Icon(Icons.close),
              onTap: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      ),
      body: new Container(
        height: 500.0,
        decoration: BoxDecoration(
          image: DecorationImage(image: NetworkImage(ll), fit: BoxFit.cover),
        ),
        child: new Center(
          child: new Text("Home Page"),
        ),
      ),
    );
  }
}
